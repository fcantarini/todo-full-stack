package com.todo.rest.webservices.restfulwebservices.todo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TodoHardcodedService {

	private static List<Todo> todos = new ArrayList<>();
	private static long idCounter = 0;
	
	static {
		todos.add(new Todo(++idCounter, "guest", "Learn to Dance", new Date(), false));
		todos.add(new Todo(++idCounter, "guest", "Learn about AWS", new Date(), false));
		todos.add(new Todo(++idCounter, "guest", "Learn to Fly", new Date(), false));
	}
	
	public List<Todo> findAll() {
		return todos;
	}
	
	public Todo deleteById(long id) {
		Todo todo = findById(id);
		if (todo != null) {
			todos.remove(todo);
		}
		return todo;
	}
	
	public Todo findById(long id) {
		return todos.stream().filter(t -> t.getId() == id).findFirst().orElse(null);
	}
	
	public Todo save(Todo todo) {
		if (todo.getId() <= 0) {
			todo.setId(++idCounter);
			todos.add(todo);
		} else {
			deleteById(todo.getId());
			todos.add(todo);
		}
		return todo;
	}
}
