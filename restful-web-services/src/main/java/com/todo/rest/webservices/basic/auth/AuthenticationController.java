package com.todo.rest.webservices.basic.auth;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AuthenticationController {
	
	@RequestMapping(method=RequestMethod.GET, path="/check-auth")
	public AuthenticationBean authenticationBean() {
		return new AuthenticationBean("You have been authenticated!");
	}
}
