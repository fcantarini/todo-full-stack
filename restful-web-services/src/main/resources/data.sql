insert into todo (id, username, description, target_date, is_done)
values (10001, 'guest', 'Learn JPA', sysdate(), false);

insert into todo (id, username, description, target_date, is_done)
values (10002, 'guest', 'Learn Data JPA', sysdate(), false);
