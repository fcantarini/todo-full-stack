import React from 'react';

export class LogoutComponent extends React.Component {
    render() {
        return (
            <div>
                <h1>You are logged out</h1>
                <div className="container">
                    Thank You for using our application.
                    </div>
            </div>
        );
    }
}
