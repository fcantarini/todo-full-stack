import React from 'react';
import { Link } from 'react-router-dom';
import HelloWorldService from '../../api/todo/HelloWorldService';

export class WelcomeComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            welcomeMessage : ""
        };
    }

    render() {
        return(
            <div>
                <h1>Welcome!</h1>
                <div className="container">
                    Welcome {this.props.match.params.name}.
                    You can manage your todos <Link to="/todos">here</Link>.
                </div>
                <div className="container">
                    Click here to get a customized welcome message.
                    <br/>
                    <button className="btn btn-success" onClick={(e) => this.retrieveWelcomeMessage(e)}>Get Welcome Message</button>
                    <p/>
                    <span>{this.state.welcomeMessage}</span>
                </div>
            </div>
        );
    }

    retrieveWelcomeMessage(e) {
        console.log("retrieveWelcomeMessage clicked");
        HelloWorldService.executeHelloWorldPathVariableService(this.props.match.params.name)
            .then(response => this.handleSuccessfulResponse(response))
            .catch(err => this.handleErrorResponse(err));
    }

    handleSuccessfulResponse(response) {
        this.setState({
            welcomeMessage : response.data.message
        });
    }

    handleErrorResponse(error) {
        console.error("ErrorResponse: " + error);
        let errorMsg = '';
        if (error.message) {
            errorMsg += error.message;
        }
        if (error.response && error.response.data) {
            errorMsg += error.response.data.message;
        }
        this.setState({
            welcomeMessage : errorMsg
        });
    }
}

