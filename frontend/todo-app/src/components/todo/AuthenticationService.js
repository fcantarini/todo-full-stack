import axios from 'axios'
import {API_URL} from '../../Constants'

class AuthenticationService {
    checkBasicAuthentication(username, password) {
        console.log(`checkBasicAuthentication for ${username}`);
        let authHeader = this.buildBasicAuthenticationHeader(username, password);
        return axios.get(`${API_URL}/check-auth`, {
            headers : {
                authorization : authHeader
            }
        });
    }

    checkJwtAuthentication(username, password) {
        console.log(`checkJwtAuthentication for ${username}`);
        return axios.post(`${API_URL}/authenticate`, {
            username : username,
            password : password
        });
    }

    registerSuccessfulLogin(username, password) {
        console.log(`AuthenticationService registerSuccessfulLogin for ${username}`);
        sessionStorage.setItem("authenticatedUser", username);
        this.setupAxiosInterceptors(username, password)
    }

    registerSuccessfulLoginForJwt(username, token) {
        console.log(`AuthenticationService registerSuccessfulLoginForJwt for ${username}`);
        sessionStorage.setItem("authenticatedUser", username);
        this.setupAxiosInterceptorsForJwt(token)
    }

    logout() {
        console.log(`AuthenticationService logout`);
        sessionStorage.removeItem("authenticatedUser");
    }

    isUserLoggedIn() {
        let user = sessionStorage.getItem('authenticatedUser');
        return (user === null ? false : true);
    }

    getUserLoggedId() {
        let user = sessionStorage.getItem('authenticatedUser');
        return user;
    }

    buildBasicAuthenticationHeader(username, password) {
        // Basic Auth sucks!
        let authHeader = "Basic " + window.btoa(`${username}:${password}`)
        return authHeader
    }

    buildJwtAuthenticationHeader(token) {
        let authHeader = `Bearer ${token}`
        return authHeader
    }

    setupAxiosInterceptors(username, password) {
        // Basic Auth sucks!
        let authHeader = this.buildBasicAuthenticationHeader(username, password);
        axios.interceptors.request.use((config) => {
            if (this.isUserLoggedIn()) {
                config.headers.authorization = authHeader
            }
            return config
        })
    }

    setupAxiosInterceptorsForJwt(token) {
        // Basic Auth sucks!
        let authHeader = this.buildJwtAuthenticationHeader(token);
        axios.interceptors.request.use((config) => {
            if (this.isUserLoggedIn()) {
                config.headers.authorization = authHeader
            }
            return config
        })
    }
}

export default new AuthenticationService();