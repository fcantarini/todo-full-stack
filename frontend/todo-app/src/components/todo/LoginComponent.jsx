import React from 'react';
import AuthenticationService from './AuthenticationService.js';

export class LoginComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "guest",
            password: "",
            hasLoginFailed: false,
            showSuccessMessage: false
        };
    }
    render() {
        return (<div className="LoginComponent">
            <h1>Login</h1>
            <div className="container">
                {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid Credentials</div>}
                {this.state.showSuccessMessage && <div>Login Successful</div>}
                User Name: <input type="text" name="username" value={this.state.username} onChange={(e) => this.handleChange(e)} />
                Password: <input type="password" name="password" value={this.state.password} onChange={(e) => this.handleChange(e)} />
                <button className="btn btn-success" onClick={(e) => this.loginClicked(e)}>Login</button>
            </div>
        </div>);
    }
    
    handleChange(e) {
        console.log(`handleChange ${e.target.name} = ${e.target.value}`);
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value,
            hasLoginFailed: false,
            showSuccessMessage: false
        });
    }

    loginClicked(e) {
        console.log("loginClicked: ", this.state);
        AuthenticationService.checkJwtAuthentication(this.state.username, this.state.password)
            .then((response) => {
                console.log("Successful")
                AuthenticationService.registerSuccessfulLoginForJwt(this.state.username, response.data.token);
                this.setState({
                    showSuccessMessage: true,
                    hasLoginFailed: false
                })
                this.props.history.push(`/welcome/${this.state.username}`)
            }).catch(() => {
                console.log("Failed");
                this.setState({
                    showSuccessMessage: false,
                    hasLoginFailed: true
                });
            })
    }
}

export default LoginComponent;