import React from 'react';

export class FooterComponent extends React.Component {
    render() {
        return (
            <footer className="footer">
                <span className="text-muted">
                    All Rights Reserved 2020 @FC   
                </span>
            </footer>);
    }
}
