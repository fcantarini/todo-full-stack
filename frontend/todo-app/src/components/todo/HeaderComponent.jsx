import React from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import AuthenticationService from './AuthenticationService.js';

export class HeaderComponent extends React.Component {
    render() {
        const userLoggedIn = AuthenticationService.isUserLoggedIn();
        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div><a href="https://cisco.udemy.com/course/full-stack-application-with-spring-boot-and-react" className="navbar-brand">ReactSpring</a></div>
                    <ul className="navbar-nav">
                        {userLoggedIn && <li><Link className="nav-link" to="/welcome/guest">Home</Link></li>}
                        {userLoggedIn && <li><Link className="nav-link" to="/todos">Todos</Link></li>}
                    </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end">
                        {!userLoggedIn && <li><Link className="nav-link" to="/login">Login</Link></li>}
                        {userLoggedIn && <li><Link className="nav-link" to="/logout" onClick={AuthenticationService.logout}>Logout</Link></li>}
                    </ul>
                </nav>
            </header>
        );
    }
}

export default withRouter(HeaderComponent);