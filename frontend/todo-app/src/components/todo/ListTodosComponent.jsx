import React from 'react';
import TodoDataService from '../../api/todo/TodoDataService.js';
import AuthentiationService from './AuthenticationService.js';
import moment from 'moment';

export class ListTodosComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: [],
            message: null
        };
    }

    render() {
        return (
            <div className="ListTodosComponent">
                <h1>List Todos</h1>
               {this.state.message && <div className="alert alert-success">{this.state.message}</div>}
                <div className="container">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Description</th>
                                <th>Target Date</th>
                                <th>Completed</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.todos.map(todo => <tr key={`todo-${todo.id}`}>
                                <td>{todo.id}</td>
                                <td>{todo.description}</td>
                                <td>{moment(todo.targetDate).format('YYYY-MM-DD')}</td>
                                <td>{todo.done.toString()}</td>
                                <td><button className="btn btn-success" onClick={(e) => this.updateButtonClicked(todo.id)}>Update</button></td>
                                <td><button className="btn btn-warning" onClick={(e) => this.deleteButtonClicked(todo.id)}>Delete</button></td>
                            </tr>)}
                        </tbody>
                    </table>
                    <div className="row">
                        <button name="Add" className="btn btn-success" onClick={(e) => this.addButtonClicked()}>Add</button>
                    </div>
                </div>
            </div>
        );
    }

    addButtonClicked() {
        console.log("addButtonClicked");
        this.props.history.push(`/todos/0`);
    }

    updateButtonClicked(id) {
        console.log("updateButtonClicked for todo id " + id);
        this.props.history.push(`/todos/${id}`);
    }

    deleteButtonClicked(id) {
        console.log("deleteButtonClicked for todo id " + id);
        let username = AuthentiationService.getUserLoggedId();
        TodoDataService.deleteTodo(username, id)
            .then(response => {
                console.log(`deleteTodo ${id} response is successfull`);
                this.setState({ message: `Todo item ${id} successfully deleted`});
            });
        this.refreshTodoList();
    }

    refreshTodoList() {
        let username = AuthentiationService.getUserLoggedId();
        TodoDataService.retrieveAllTodos(username)
            .then(response => {
                console.log(response);
                this.setState({ todos: response.data});
            });
    }

    componentDidMount() {
        console.log("componentDidMount");
        this.refreshTodoList();
    }

    componentWillUnmount() {
        console.log("componentWillUnmount");
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log(`shouldComponentUpdate, nextProps ${nextProps}, nextState ${nextState}`);
        return true;
    }
}

export default ListTodosComponent;