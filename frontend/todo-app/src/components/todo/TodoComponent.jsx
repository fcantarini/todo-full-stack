import { Formik, Form, Field, ErrorMessage } from 'formik';
import moment from 'moment';
import React from 'react';
import TodoDataService from '../../api/todo/TodoDataService.js';
import AuthenticationService from './AuthenticationService.js';

export class TodoComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            description: '',
            targetDate: moment(new Date()).format('YYYY-MM-DD'),
            completed: false
        };
    }

    render() {
        return(
            <div className="container">
                <h1>Todo</h1>
                <Formik validateOnBlur={false} validateOnChange={false} enableReinitialize={true}
                    initialValues={this.state} onSubmit={(values) => this.onSubmit(values)} validate={(values) => this.validate(values)}>
                    {
                        (props) => (
                            <Form>
                                <ErrorMessage name="description" component="div" className="alert alert-warning"/>
                                <fieldset className="form-group">
                                    <label>Description</label>
                                    <Field className="form-control" type="text" name="description"></Field>
                                </fieldset>
                                <fieldset className="form-group">
                                    <label>Target Date</label>
                                    <Field className="form-control" type="date" name="targetDate" ></Field>
                                </fieldset>
                                <button className="btn btn-success" type="submit">Save</button>
                            </Form>
                        )
                    }
                </Formik>
            </div>
        );
    }

    componentDidMount() {
        if (this.state.id <= 0) {
            return;
        }
        let username = AuthenticationService.getUserLoggedId();
        TodoDataService.getTodo(username, this.state.id)
            .then(response => this.setState({
                description : response.data.description,
                targetDate : moment(response.data.targetDate).format('YYYY-MM-DD')
            }));
    }

    onSubmit(values) {
        console.log("Submit ", values);
        let username = AuthenticationService.getUserLoggedId();
        let todo = {
            id : this.state.id,
            description : values.description,
            targetDate : values.targetDate
        }
        if (this.state.id <= 0) {
            TodoDataService.addTodo(username, todo)
                .then(() => this.props.history.push("/todos"));
        } else {
            TodoDataService.updateTodo(username, this.state.id, todo)
                .then(() => this.props.history.push("/todos"));
        }
    }

    validate(values) {
        console.log("Validate ", values);
        let errors = {};
        if (!values.description) {
            errors.description = "Enter a description";
        } else if (values.description.length < 4) {
            errors.description = "Description should have at least 4 characters";
        }
        if (!moment(values.targetDate).isValid()) {
            errors.description = "Enter a valid target date";
        }
        return errors;
    }
}

export default TodoComponent;