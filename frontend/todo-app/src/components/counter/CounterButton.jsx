import React from 'react';
import PropTypes from 'prop-types';
import './Counter.css'

class CounterButton extends React.Component {
    render() {
        return (
            <div className="Counter">
              <button onClick={this.increment}>+{this.props.by}</button>
              <button onClick={this.decrement}>-{this.props.by}</button>
            </div>
          );  
    }

    increment = () => {
        this.props.counter.increment(this.props.by);
    }

    decrement = () => {
        this.props.counter.decrement(this.props.by);
    }
}

CounterButton.defaultProps = {
    by : 1
}

CounterButton.propTypes = {
    // number to increment or decrement by
    by : PropTypes.number,
    // Counter component
    counter : PropTypes.object
}

export default CounterButton;
