import React from 'react';
import CounterButton from './CounterButton';
import ResetButton from './ResetButton';
import './Counter.css';

class Counter extends React.Component {
    constructor() {
        super();
        this.state = {
            counter : 0
        };
        //this.increment = this.increment.bind(this);
    }

    render() {
      return (
        <div className="Counter">
          <CounterButton counter={this}/>
          <CounterButton counter={this} by={5}/>
          <CounterButton counter={this} by={10}/>
          <span className="count">{this.state.counter}</span>
          <ResetButton resetMethod={this.reset}/>
        </div>
      );  
    }

    // arrow function instead of increment() {} so we don't need to bind this
    increment = (by) => {
        console.log(`Counter increment by ${by}`);
        // we can use this.state ...
        this.setState({
            counter: this.state.counter + by
        });
    }

    decrement = (by) => {
        console.log(`Counter decrement by ${by}`);
        // ... or we can use the previous state
        this.setState((state) => {
            return {
                counter : state.counter - by
            }
        });
    }

    reset = () => {
        this.setState({
            counter: 0
        });
    }
  }

  export default Counter;