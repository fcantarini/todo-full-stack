import React from 'react';
import PropTypes from 'prop-types';
import './Counter.css'

class ResetButton extends React.Component {

    render() {
        return (
            <div className="ResetButton">
              <button className="reset" onClick={this.reset}>Reset</button>
            </div>
          );  
    }

    reset = () => {
        this.props.resetMethod();
    }
}

ResetButton.propTypes = {
    // function to call to reset counter
    resetMethod : PropTypes.func
}

export default ResetButton;
