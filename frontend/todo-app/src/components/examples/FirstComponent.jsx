import React from 'react';

class FirstComponent extends React.Component {
  render() {
    return (
      <div className="FirstComponent">
        First Component
      </div>
    );  
  }
}

export default FirstComponent;
