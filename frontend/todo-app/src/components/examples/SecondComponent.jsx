import React from 'react';

class SecondComponent extends React.Component {
  render() {
    return (
      <div className="SecondComponent">
        Second Component
      </div>
    );  
  }
}

export default SecondComponent;
