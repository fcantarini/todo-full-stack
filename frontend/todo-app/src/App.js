import React from 'react';
import logo from './logo.svg';
import './App.css';
import FirstComponent from './components/examples/FirstComponent';
import SecondComponent from './components/examples/SecondComponent';
import ThirdComponent from './components/examples/ThirdComponent';
import Counter from './components/counter/Counter';
import TodoApp from './components/todo/TodoApp';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        {/*<Counter/>*/}
        <TodoApp/>
      </div>
    );  
  }
}

class LearningComponents extends React.Component {
  render() {
    return (
      <div className="LearningComponents">
        My Hello World
        <FirstComponent/>
        <SecondComponent/>
        <ThirdComponent/>
      </div>
    );
  }
}

export default App;
