import axios from 'axios'
import {API_URL} from '../../Constants'

class HelloWorldService {
    executeHelloWorldService() {
        console.log("executeHelloWorldService");
        return axios.get(`${API_URL}/hello-world`);
    }

    executeHelloWorldBeanService() {
        console.log("executeHelloWorldBeanService");
        return axios.get(`${API_URL}/hello-world-bean`);
    }

    executeHelloWorldPathVariableService(name) {
        console.log("executeHelloWorldPathVariableService");
        return axios.get(`${API_URL}/hello-world/path-variable/${name}`)
    }
}

export default new HelloWorldService();