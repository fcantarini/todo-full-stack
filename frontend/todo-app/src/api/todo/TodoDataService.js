import axios from 'axios'
import { API_URL, USERS_API_URL } from '../../Constants'

class TodoDataService {
    retrieveAllTodos(name) {
        console.log("retrieveAllTodos");
        return axios.get(`${USERS_API_URL}/${name}/todos`);
    }

    getTodo(name, id) {
        console.log("getTodo");
        return axios.get(`${USERS_API_URL}/${name}/todos/${id}`);
    }

    deleteTodo(name, id) {
        console.log("deleteTodo");
        return axios.delete(`${USERS_API_URL}/${name}/todos/${id}`);
    }

    updateTodo(name, id, todo) {
        console.log("updateTodo");
        return axios.put(`${USERS_API_URL}/${name}/todos/${id}`, todo);
    }

    addTodo(name, todo) {
        console.log("addTodo");
        return axios.post(`${USERS_API_URL}/${name}/todos`, todo);
    }
}

export default new TodoDataService();